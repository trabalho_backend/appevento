package com.cotemig.appEvento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.appEvento.model.Agenda;

@Repository("agendaRepository")
public interface AgendaRepository extends JpaRepository<Agenda, Integer> {
	 // method
}