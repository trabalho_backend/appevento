package com.cotemig.appEvento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.appEvento.model.Evento;

@Repository("eventoRepository")
public interface EventoRepository extends JpaRepository<Evento, Integer> {
	 // method
}