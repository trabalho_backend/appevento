package com.cotemig.appEvento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.appEvento.model.Ingresso;

@Repository("ingressoRepository")
public interface IngressoRepository extends JpaRepository<Ingresso, Integer> {
	 // method
}