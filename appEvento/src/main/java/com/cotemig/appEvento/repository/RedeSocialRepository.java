package com.cotemig.appEvento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.appEvento.model.RedeSocial;

@Repository("redeSocialRepository")
public interface RedeSocialRepository extends JpaRepository<RedeSocial, Integer> {
	 // method
}