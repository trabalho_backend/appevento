package com.cotemig.appEvento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.appEvento.model.ResponsavelEvento;

@Repository("responsaveleventoRepository")
public interface ResponsavelEventoRepository extends JpaRepository<ResponsavelEvento, Integer> {
	 // method
}