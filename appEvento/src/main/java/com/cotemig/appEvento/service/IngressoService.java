package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;
import com.cotemig.appEvento.model.Ingresso;

public interface IngressoService {
	 Optional<Ingresso> getIngressoById(Integer id);
	 List<Ingresso> getAllIngressos();
	 void deleteAllIngressos();
	 void deleteIngressoById(Integer id);
	 void updateIngressoById(Integer id, Ingresso Ingresso);
	 void updateIngresso(Ingresso Ingresso);
	 void insertIngresso(Ingresso Ingresso);
}