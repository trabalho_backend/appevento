package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.appEvento.model.Ingresso;
import com.cotemig.appEvento.repository.IngressoRepository;

@Service("IngressoService")
public class IngressoImpl implements IngressoService{

	@Autowired
	IngressoRepository ingressoRepository; 
	
	@Override
	public Optional<Ingresso> getIngressoById(Integer id) {
		return ingressoRepository.findById(id);
	}

	@Override
	public List<Ingresso> getAllIngressos() {
		return ingressoRepository.findAll();
	}

	@Override
	public void deleteAllIngressos() {
		ingressoRepository.deleteAll();
	}

	@Override
	public void deleteIngressoById(Integer id) {
		ingressoRepository.deleteById(id);
	}

	@Override
	public void updateIngressoById(Integer id, Ingresso Ingresso) {
		Optional<Ingresso> getIngresso = getIngressoById(id);
		getIngresso.get().setLink_ingresso(Ingresso.getLink_ingresso());
		 
		ingressoRepository.save(Ingresso);
	}

	@Override
	public void updateIngresso(Ingresso Ingresso) {
		ingressoRepository.save(Ingresso);
	}

	@Override
	public void insertIngresso(Ingresso Ingresso) {
		ingressoRepository.save(Ingresso);
	}
}
