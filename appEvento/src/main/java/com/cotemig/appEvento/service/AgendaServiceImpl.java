package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.appEvento.model.Agenda;
import com.cotemig.appEvento.repository.AgendaRepository;

@Service("agendaService")
public class AgendaServiceImpl implements AgendaService{

	@Autowired
	AgendaRepository agendaRepository; 
	
	@Override
	public Optional<Agenda> getAgendaById(Integer id) {
		return agendaRepository.findById(id);
	}



	@Override
	public void deleteAgendaById(Integer id) {
		agendaRepository.deleteById(id);
	}

	@Override
	public void updateAgendaById(Integer id, Agenda agenda) {
		Optional<Agenda> getAgenda = getAgendaById(id);
		agendaRepository.save(agenda);
	}

	@Override
	public void updateAgenda(Agenda agenda) {
		agendaRepository.save(agenda);
	}

	@Override
	public void insertAgenda(Agenda agenda) {
		agendaRepository.save(agenda);
	}
}
