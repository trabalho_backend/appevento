package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;
import com.cotemig.appEvento.model.RedeSocial;

public interface RedeSocialService {
	 Optional<RedeSocial> getRedeSocialById(Integer id);
	 List<RedeSocial> getAllRedeSociais();
	 void deleteAllRedeSociais();
	 void deleteRedeSocialById(Integer id);
	 void updateRedeSocialById(Integer id, RedeSocial RedeSocial);
	 void updateRedeSocial(RedeSocial RedeSocial);
	 void insertRedeSocial(RedeSocial RedeSocial);
}