package com.cotemig.appEvento.service;

import java.util.Optional;

import com.cotemig.appEvento.model.Agenda;

public interface AgendaService {
	 Optional<Agenda> getAgendaById(Integer id);
	 void deleteAgendaById(Integer id);
	 void updateAgendaById(Integer id, Agenda agenda);
	 void updateAgenda(Agenda agenda);
	 void insertAgenda(Agenda agenda);
}