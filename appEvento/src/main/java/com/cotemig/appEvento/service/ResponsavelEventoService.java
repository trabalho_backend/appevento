package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import com.cotemig.appEvento.model.ResponsavelEvento;

public interface ResponsavelEventoService {
	 Optional<ResponsavelEvento> getResponsavelEventoById(Integer id);
	 List<ResponsavelEvento> getAllResponsavelEventos();
	 void deleteAllResponsavelEventos();
	 void deleteResponsavelEventoById(Integer id);
	 void updateResponsavelEventoById(Integer id, ResponsavelEvento responsavelevento);
	 void updateResponsavelEvento(ResponsavelEvento responsavelevento);
	 void insertResponsavelEvento(ResponsavelEvento responsavelevento);
}
