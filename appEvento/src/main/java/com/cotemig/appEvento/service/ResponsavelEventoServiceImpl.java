package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.appEvento.model.ResponsavelEvento;
import com.cotemig.appEvento.repository.ResponsavelEventoRepository;

@Service("responsaveleventoService")
public class ResponsavelEventoServiceImpl implements ResponsavelEventoService{

	@Autowired
	ResponsavelEventoRepository responsaveleventoRepository; 
	
	@Override
	public Optional<ResponsavelEvento> getResponsavelEventoById(Integer id) {
		return responsaveleventoRepository.findById(id);
	}

	@Override
	public List<ResponsavelEvento> getAllResponsavelEventos() {
		return responsaveleventoRepository.findAll();
	}

	@Override
	public void deleteAllResponsavelEventos() {
		responsaveleventoRepository.deleteAll();
	}

	@Override
	public void deleteResponsavelEventoById(Integer id) {
		responsaveleventoRepository.deleteById(id);
	}

	@Override
	public void updateResponsavelEventoById(Integer id, ResponsavelEvento responsavelevento) {
		Optional<ResponsavelEvento> getResponsavelEvento = getResponsavelEventoById(id);
		getResponsavelEvento.get().setNome(responsavelevento.getNome());
		 
		responsaveleventoRepository.save(responsavelevento);
	}

	@Override
	public void updateResponsavelEvento(ResponsavelEvento responsavelevento) {
		responsaveleventoRepository.save(responsavelevento);
	}

	@Override
	public void insertResponsavelEvento(ResponsavelEvento responsavelevento) {
		responsaveleventoRepository.save(responsavelevento);
	}
}
