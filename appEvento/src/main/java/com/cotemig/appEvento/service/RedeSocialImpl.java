package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.appEvento.model.RedeSocial;
import com.cotemig.appEvento.repository.RedeSocialRepository;

@Service("redeSocialService")
public class RedeSocialImpl implements RedeSocialService{

	@Autowired
	RedeSocialRepository redeSocialRepository; 
	
	@Override
	public Optional<RedeSocial> getRedeSocialById(Integer id) {
		return redeSocialRepository.findById(id);
	}

	@Override
	public List<RedeSocial> getAllRedeSociais() {
		return redeSocialRepository.findAll();
	}

	@Override
	public void deleteAllRedeSociais() {
		redeSocialRepository.deleteAll();
	}

	@Override
	public void deleteRedeSocialById(Integer id) {
		redeSocialRepository.deleteById(id);
	}

	@Override
	public void updateRedeSocialById(Integer id, RedeSocial RedeSocial) {
		Optional<RedeSocial> getRedeSocial = getRedeSocialById(id);
		getRedeSocial.get().setNome(RedeSocial.getNome());
		 
		redeSocialRepository.save(RedeSocial);
	}

	@Override
	public void updateRedeSocial(RedeSocial RedeSocial) {
		redeSocialRepository.save(RedeSocial);
	}

	@Override
	public void insertRedeSocial(RedeSocial RedeSocial) {
		redeSocialRepository.save(RedeSocial);
	}
}
