package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.appEvento.model.Evento;
import com.cotemig.appEvento.repository.EventoRepository;

@Service("eventoService")
public class EventoServiceImpl implements EventoService{

	@Autowired
	EventoRepository eventoRepository; 
	
	@Override
	public Optional<Evento> getEventoById(Integer id) {
		return eventoRepository.findById(id);
	}

	@Override
	public List<Evento> getAllEventos() {
		return eventoRepository.findAll();
	}

	@Override
	public void deleteAllEventos() {
		eventoRepository.deleteAll();
	}

	@Override
	public void deleteEventoById(Integer id) {
		eventoRepository.deleteById(id);
	}

	@Override
	public void updateEventoById(Integer id, Evento evento) {
		Optional<Evento> getEvento = getEventoById(id);
		getEvento.get().setNome(evento.getNome());
		 
		eventoRepository.save(evento);
	}

	@Override
	public void updateEvento(Evento evento) {
		eventoRepository.save(evento);
	}

	@Override
	public void insertEvento(Evento evento) {
		eventoRepository.save(evento);
	}
}
