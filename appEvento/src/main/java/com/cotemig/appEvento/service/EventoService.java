package com.cotemig.appEvento.service;

import java.util.List;
import java.util.Optional;

import com.cotemig.appEvento.model.Evento;

public interface EventoService {
	 Optional<Evento> getEventoById(Integer id);
	 List<Evento> getAllEventos();
	 void deleteAllEventos();
	 void deleteEventoById(Integer id);
	 void updateEventoById(Integer id, Evento evento);
	 void updateEvento(Evento evento);
	 void insertEvento(Evento evento);
}