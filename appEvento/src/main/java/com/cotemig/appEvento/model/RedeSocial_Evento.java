package com.cotemig.appEvento.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RedeSocial_Evento {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	private String username;
	private String link;
	
	@ManyToOne
    @JoinColumn(name="evento_id", nullable=false)
    private Evento evento;
	
	@ManyToOne
    @JoinColumn(name="redesocial_id", nullable=false)
    private RedeSocial redesocial;

	protected Integer getId() {
		return id;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected String getUsername() {
		return username;
	}

	protected void setUsername(String username) {
		this.username = username;
	}

	protected String getLink() {
		return link;
	}

	protected void setLink(String link) {
		this.link = link;
	}

	protected Evento getEvento() {
		return evento;
	}

	protected void setEvento(Evento evento) {
		this.evento = evento;
	}

	protected RedeSocial getRedesocial() {
		return redesocial;
	}

	protected void setRedesocial(RedeSocial redesocial) {
		this.redesocial = redesocial;
	}
}
