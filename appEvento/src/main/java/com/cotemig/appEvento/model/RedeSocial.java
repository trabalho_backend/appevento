package com.cotemig.appEvento.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class RedeSocial {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	private String nome;
	
	@OneToMany(mappedBy="redesocial")
	private Set<RedeSocial_Evento> redesocial_eventos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<RedeSocial_Evento> getRedesocial_eventos() {
		return redesocial_eventos;
	}

	public void setRedesocial_eventos(Set<RedeSocial_Evento> redesocial_eventos) {
		this.redesocial_eventos = redesocial_eventos;
	}
}
