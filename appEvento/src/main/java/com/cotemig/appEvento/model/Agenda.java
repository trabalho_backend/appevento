package com.cotemig.appEvento.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Agenda {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	
    @OneToOne(mappedBy = "agenda")
    private Usuario usuario;
    
    @OneToMany(mappedBy="agenda")
	private Set<Agenda_Evento> agenda_eventos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	protected Set<Agenda_Evento> getAgenda_eventos() {
		return agenda_eventos;
	}

	protected void setAgenda_eventos(Set<Agenda_Evento> agenda_eventos) {
		this.agenda_eventos = agenda_eventos;
	}
}
