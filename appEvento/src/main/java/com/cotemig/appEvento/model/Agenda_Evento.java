package com.cotemig.appEvento.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Agenda_Evento {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	
	@ManyToOne
    @JoinColumn(name="agenda_id", nullable=false)
    private Agenda agenda;
	
	@ManyToOne
    @JoinColumn(name="evento_id", nullable=false)
    private Evento evento;

	protected Integer getId() {
		return id;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected Agenda getAgenda() {
		return agenda;
	}

	protected void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}

	protected Evento getEvento() {
		return evento;
	}

	protected void setEvento(Evento evento) {
		this.evento = evento;
	}
	
}
