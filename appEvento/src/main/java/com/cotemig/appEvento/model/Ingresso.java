package com.cotemig.appEvento.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Ingresso {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	private String link_ingresso;
	
	
    @OneToOne(mappedBy = "ingresso")
    private Evento evento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLink_ingresso() {
		return link_ingresso;
	}

	public void setLink_ingresso(String link_ingresso) {
		this.link_ingresso = link_ingresso;
	}

	protected Evento getEvento() {
		return evento;
	}

	protected void setEvento(Evento evento) {
		this.evento = evento;
	}

	
}
