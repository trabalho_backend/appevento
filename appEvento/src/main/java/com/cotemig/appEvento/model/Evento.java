package com.cotemig.appEvento.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Evento {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	private String nome;
	private Date data;
	
	@ManyToOne
    @JoinColumn(name="responsavelevento_id", nullable=false)
    private ResponsavelEvento responsavelevento;
	
    @OneToOne(mappedBy = "evento")
    private Ingresso ingresso;
	
	@OneToMany(mappedBy="evento")
	private Set<RedeSocial_Evento> redesocial_eventos;
	
	@OneToMany(mappedBy="evento")
	private Set<Agenda_Evento> agenda_eventos;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public ResponsavelEvento getResponsavelevento() {
		return responsavelevento;
	}

	public void setResponsavelevento(ResponsavelEvento responsavelevento) {
		this.responsavelevento = responsavelevento;
	}

	protected Ingresso getIngresso() {
		return ingresso;
	}

	protected void setIngresso(Ingresso ingresso) {
		this.ingresso = ingresso;
	}

	protected Set<RedeSocial_Evento> getRedesocial_eventos() {
		return redesocial_eventos;
	}

	protected void setRedesocial_eventos(Set<RedeSocial_Evento> redesocial_eventos) {
		this.redesocial_eventos = redesocial_eventos;
	}

	protected Set<Agenda_Evento> getAgenda_eventos() {
		return agenda_eventos;
	}

	protected void setAgenda_eventos(Set<Agenda_Evento> agenda_eventos) {
		this.agenda_eventos = agenda_eventos;
	}	
}
